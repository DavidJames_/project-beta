# CarCar

Team:

* Dane - Which microservice?
    Service
* David - Which microservice?
    Sales
## Design

## Service microservice

Created models for Technician and Appointment entries with an AutomobileVO for comparing vin numbers between autos and appointments for "Is VIP?" data later on
Created view functions with GET, POST, AND DELETE requests for both Technicians and Appointments with special PUT requests for Appointments to mark them as "canceled" or "finished"
Filled out the poller and made changes to the settings to handle CORS errors and to allow for port-to-port communication
Created list pages and create forms for Appointments and Technicians with a service history page in REACT that shows all appointments in the database regardless of status (The appointment list itself only showing appointments with "created" status)
Added buttons to cancel or finish appointment on the list directly, immediately removing the entry from the list on click (but not from the history), and also added checkmarks for people who schedule an appointment for a vehicle vin that matches the vin of a vehicle in the inventory (Is VIP?)
Created the model form and model list for inventory frontend
Made the navbar dynamic with dropdowns to solve the clutter of having ~18 buttons for each form/list down to 8 dropdown button categories for more-organized and cleaner-looking navigation
* Also made the navbar collapse vertically into a single column of dropdowns, almost like a list of dropdowns to, again, avoid clutter when the webpage orientation is scrunched
Went back and added universally-styled headers to all forms and lists for stylistic continuity across the entire web app

## Sales microservice
Created AutomobileVO, as well as Salesperson, Customer, Sale, and their respective JSON encoders. Integration with inventory occurs mostly on front-end. Added Manufacturer form, manufacturer, models, automobiles, automobile form. Also customized mainpage and restructured apps.js to allow for more customization outside of the container. Any time a sale is made, the selected automobile associated with the VIN is updated as sold.
