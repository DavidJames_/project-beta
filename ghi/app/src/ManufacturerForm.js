import React, {useState,} from 'react';
import { useNavigate } from 'react-router-dom';

function ManufacturerForm() {
    const [name, setName] = useState('')
    const navigate = useNavigate()

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}

        data.name = name

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(manufacturerUrl, fetchConfig)
        if (response.ok) {
            const newManufacturer = await response.json()
            console.log(newManufacturer)

            setName('')
            navigate('/manufacturers')
        }
    }

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }
    return (
        <div key='form container' className='my-5 text-center container'>
            <div key='row' className='row'>
                <div key='column' className='offset-3 col-6'>
                    <div key='card' className='shadow p-4 mt-4'>
                        <h1>Add a Manufacturer</h1>
                        <form onSubmit={handleSubmit} id='create-manufacturer-form'>
                            <div key='nameform' className='form-floating mb-3'>
                                <input value={name} onChange={handleNameChange} placeholder='name' required type='text' name='ame' id='name' className='form-control'></input>
                                <label htmlFor='name'>Manufacturer name...</label>
                            </div>
                            <button className='btn btn-primary'>Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default ManufacturerForm
