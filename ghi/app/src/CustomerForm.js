import React, {useState,} from 'react';
import { useNavigate } from 'react-router-dom';

function CustomerForm() {
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')
    const [address, setAddress] = useState('')
    const navigate = useNavigate()
    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.first_name = firstName
        data.last_name = lastName
        data.phone_number = phoneNumber
        data.address = address

        const customerUrl = 'http://localhost:8090/api/customers/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(customerUrl, fetchConfig)
        if (response.ok) {
            const newCustomer = await response.json()
            console.log(newCustomer)

            setFirstName('')
            setLastName('')
            setPhoneNumber('')
            setAddress('')
            navigate('/customers')
        }
    }

    const handleFirstNameChange = (event) => {
        const value = event.target.value
        setFirstName(value)

    }
    const handleLastNameChange = (event) => {
        const value = event.target.value
        setLastName(value)
    }
    const handlePhoneNumberChange = (event) => {
        const value = event.target.value
        setPhoneNumber(value)
    }
    const handleAddressChange = (event) => {
        const value = event.target.value
        setAddress(value)
    }

    return (
        <div key='form container' className='my-5 text-center container'>
            <div key='row' className='row'>
                <div key='column' className='offset-3 col-6'>
                    <div key='card' className='shadow p-4 mt-4'>
                        <h1>Add a Customer</h1>
                        <form onSubmit={handleSubmit} id='create-customer-form'>
                            <div key='firstnameform' className='form-floating mb-3'>
                                <input value={firstName} onChange={handleFirstNameChange} placeholder='First name' required type='text' name='first_name' id='first_name' className='form-control'></input>
                                <label htmlFor='first_name'>First name</label>
                            </div>
                            <div key='lastnameform' className='form-floating mb-3'>
                                <input value={lastName} onChange={handleLastNameChange} placeholder='Last name' required type='text' name='last_name' id='last_name' className='form-control'></input>
                                <label htmlFor='first_name'>Last name</label>
                            </div>
                            <div key='phonenumberform' className='form-floating mb-3'>
                                <input value={phoneNumber} onChange={handlePhoneNumberChange} placeholder='Phone number' required type='text' name='phone_number' id='phone_number' className='form-control'></input>
                                <label htmlFor='phone_number'>Phone number</label>
                            </div>
                            <div key='addressform' className='form-floating mb-3'>
                                <input value={address} onChange={handleAddressChange} placeholder='Address' required type='text' name='address' id='address' className='form-control'></input>
                                <label htmlFor='address'>Address</label>
                            </div>
                            <button className='btn btn-primary'>Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default CustomerForm
