import { useEffect, useState } from 'react';

function ServiceHistory() {
    const [appointments, setAppointments] = useState([])
    const [automobiles, setAutomobiles] = useState([])
    const [search, setSearch] = useState('')
    console.log(search)

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        }
    }

    const getAuto = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
            console.log(data.autos)
        }

    }

    // const handleFilter = () => {
    //     appointments
    //     .filter((appointment)=>{
    //         if (search === ""){
    //             return appointment
    //         } else if (appointment.vin.includes(search)){
    //             return appointment.vin.includes(search)
    //         }
    //     })
    // }

    useEffect(()=>{
        getData()
        getAuto()
    }, [])


    return (
            <div className="my-5">
                <div>
                    <h1 className="display-5 fw-bold text-center">Service History</h1>
                    <div className="my-4 input-group">
                        <div className="form-outline">
                            <input type="search" onChange={(e) => setSearch(e.target.value)} placeholder="Search by VIN..." className="form-control" />
                        </div>
                    </div>
                </div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Is VIP?</th>
                            <th>Customer</th>
                            <th>Date/Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments
                            .filter(appointment => {
                                return search === '' ? appointment : appointment.vin.includes(search);
                            })
                            .map(appointment => {
                                return (
                                <tr key={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    <td>
                                        {automobiles.map(automobile => {
                                            if (appointment.vin === automobile.vin) {
                                                return '✔️'
                                            } else {
                                                return (null);
                                            }
                                        })}
                                    </td>
                                    <td>{appointment.customer}</td>
                                    <td>{appointment.date_time}</td>
                                    <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                    <td>{appointment.reason}</td>
                                    <td>{appointment.status}</td>
                                </tr>
                                );
                        })}
                    </tbody>
                </table>
            </div>
    );
}
export default ServiceHistory;
