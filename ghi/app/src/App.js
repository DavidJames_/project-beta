import { BrowserRouter, Routes, Route} from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianList from './TechnicianList';
import AppointmentList from './AppointmentList';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './AppointmentForm';
import ServiceHistory from './ServiceHistory';

import SalespeopleList from './Salespeople';
import SalespersonForm from './SalespersonForm';
import CustomerList from './Customers';
import CustomerForm from './CustomerForm';
import SalesList from './Sales';
import SalesForm from './Salesform';
import AutomobileList from './Automobiles';
import AutomobileForm from './AutomobileForm';
import ManufacturerList from './Manufacturers';
import ManufacturerForm from './ManufacturerForm';
import ModelsList from './Models';
import ModelForm from './ModelForm';
import SalespersonListHistory from './Saleshistory';
import Skyline from './assets/Skyline.svg'

function Container({ children }) {
  return <div className='container'>{children}</div>;
}

export default App;

function App() {
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/technicians" element={<Container><TechnicianList /></Container>} />
          <Route path="/technicians/new" element={<Container><TechnicianForm /></Container>} />
          <Route path="/appointments" element={<Container><AppointmentList /></Container>} />
          <Route path="/appointments/new" element={<Container><AppointmentForm /></Container>} />
          <Route path="/appointments/history" element={<Container><ServiceHistory /></Container>} />
          <Route path='/salespeople' element={<Container><SalespeopleList /></Container>} />
          <Route path='/salespeople/new' element={<Container><SalespersonForm /></Container>} />
          <Route path='/customers' element={<Container><CustomerList /></Container>} />
          <Route path='/customers/new' element={<Container><CustomerForm /></Container>} />
          <Route path='/sales' element={<Container><SalesList /></Container>} />
          <Route path='/sales/new' element={<Container><SalesForm /></Container>} />
          <Route path='/sales/history' element={<Container><SalespersonListHistory /></Container>} />
          <Route path='/automobiles' element={<Container><AutomobileList /></Container>} />
          <Route path='/automobiles/new' element={<Container><AutomobileForm /></Container>} />
          <Route path='/manufacturers' element={<Container><ManufacturerList /></Container>} />
          <Route path='/manufacturers/new' element={<Container><ManufacturerForm /></Container>} />
          <Route path='/models' element={<Container><ModelsList /></Container>} />
          <Route path='/models/new' element={<Container><ModelForm /></Container>} />
        </Routes>
    </BrowserRouter>
  );
}
