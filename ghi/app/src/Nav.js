import { NavLink } from 'react-router-dom';
import HomeLogoSimple from './assets/HomeLogoSimple.svg';
import FormLogoSimple from './assets/FormLogoSimple.svg';
import React, {useEffect, useState,} from 'react';
import { useLocation } from "react-router-dom";
import './index.css';


function Nav() {
  const location = useLocation();
  const [fontColor, setFontColor] = useState('');
  const [logo, setLogo] = useState('');

  const getPageLocation = () => {
    if (location.pathname === "/") {
      setFontColor("nav-link dropdown-toggle nav-style active mx-3");
      setLogo(`${HomeLogoSimple}`);
    } else {
      setFontColor("nav-link dropdown-toggle nav-style active text-black mx-3");
      setLogo(`${FormLogoSimple}`);
    }
    console.log(location)
  }

  useEffect(() => {
    getPageLocation(location)
  }, [location])

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-transparent">
      <div className="container-fluid">
        <NavLink className="navbar-brand my-4" to="/"><img className='position-absolute fixed-top mx-4 my-4' src={logo} alt='carlogosimple' style={{width:'40px', height:'40px'}} /></NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse flex-column" id="navbarSupportedContent">
          <ul className="navbar-nav me-2  mb-2 mb-lg-0 flex">
            <li className="nav-item dropdown">
              <a className={fontColor} id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  Technicians
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><NavLink className="dropdown-item" to="/technicians" >Technicians</NavLink></li>
                <li><NavLink className="dropdown-item" to="/technicians/new" >Add a Technician</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className={fontColor} id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  Appointments
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><NavLink className="dropdown-item" to="/appointments" >Appointments</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/new" >Create a Service Appointment</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/history" >Service History</NavLink></li>
              </ul>
            </li>
            <li className='nav-item dropdown'>
              <a className={fontColor} id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  Customers
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className="dropdown-item" to='/customers'>Customers</NavLink>
                <NavLink className="dropdown-item" to='/customers/new'>Add a Customer</NavLink>
              </div>
            </li>
            <li className='nav-item dropdown'>
              <a className={fontColor} id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  Sales
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className="dropdown-item" to='/sales'>Sales</NavLink>
                <NavLink className="dropdown-item" to='/sales/new'>Add a Sale</NavLink>
                <NavLink className="dropdown-item" to='/salespeople'>Salespeople</NavLink>
                <NavLink className="dropdown-item" to='/salespeople/new'>Add a Salesperson</NavLink>
                <NavLink className="dropdown-item" to='/sales/history'>Sales History</NavLink>
              </div>
            </li>
            <li className='nav-item dropdown'>
              <a className={fontColor} id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  Automobiles
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className="dropdown-item" to='/automobiles'>Automobiles</NavLink>
                <NavLink className="dropdown-item" to='/automobiles/new'>Add an Automobile</NavLink>
              </div>
            </li>
            <li className='nav-item dropdown'>
              <a className={fontColor} id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  Manufacturers
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className="dropdown-item" to='/manufacturers'>Manufacturers</NavLink>
                <NavLink className="dropdown-item" to='/manufacturers/new'>Add a Manufacturer</NavLink>
              </div>
            </li>
            <li className='nav-item dropdown'>
              <a className={fontColor} id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  Models
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className="dropdown-item" to='/models'>Models</NavLink>
                <NavLink className="dropdown-item" to='/models/new'>Add a Model</NavLink>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
