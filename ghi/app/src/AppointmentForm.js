import React, {useEffect, useState} from 'react';
import { useNavigate } from 'react-router-dom';

function AppointmentForm() {
    const [technicians, setTechnicians] = useState([]);
    const [dateTime, setDateTime] = useState('');
    const [reason, setReason] = useState('');
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [technician, setTechnician] = useState('');
    const navigate = useNavigate()

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.date_time = dateTime;
        data.reason = reason;
        data.vin = vin;
        data.customer = customer;
        data.technician = technician

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();
            console.log(newAppointment);

            setDateTime('');
            setReason('');
            setVin('');
            setCustomer('');
            setTechnician('');
            navigate('/appointments')

        }
    }

    const handleDateTimeChange = (event) => {
        const value = event.target.value;
        setDateTime(value);
    }

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/'

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }

    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="my-5 text-center container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create an appointment</h1>
                        <form onSubmit={handleSubmit} id="create-technician-form">
                            <div className="form-floating mb-3">
                                <input value={dateTime} onChange={handleDateTimeChange} placeholder="Date/Time" required type="datetime-local" name="date_time" id="date_time" className="form-control"></input>
                                <label htmlFor="date_time">Date/Time</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={reason} onChange={handleReasonChange} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control"></input>
                                <label htmlFor="reason">Reason</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={vin} onChange={handleVinChange} placeholder="Vin #" required type="text" name="vin" id="vin" className="form-control"></input>
                                <label htmlFor="vin">Vin #</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={customer} onChange={handleCustomerChange} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control"></input>
                                <label htmlFor="customer">Customer</label>
                            </div>
                            <div className="mb-3">
                                <select value={technician} onChange={handleTechnicianChange} required name="technician" id="technician" className="form-select">
                                    <option value="">Choose a technician</option>
                                    {technicians.map(technician => {
                                        return (
                                            <option key={technician.id} value={technician.id}>
                                                <div>{technician.first_name} {technician.last_name}</div>
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )

}


export default AppointmentForm;
