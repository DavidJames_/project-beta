import { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';


function AppointmentList() {
    const [appointments, setAppointments] = useState([])
    const [automobiles, setAutomobiles] = useState([])
    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        }
    }

    const getAuto = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
            console.log(data.autos)
        }
        
    }
    
    useEffect(()=>{
        getData()
        getAuto()
    }, [])
    
    // const filterAppointments = appointments.filter(appointment => appointment.status === "in progress")
    
    return (
            <div className="my-5">
                <div>
                    <h1 className="display-5 fw-bold text-center">Service Appointments</h1>
                    <div className="col-lg-6 mx-auto">
                    <p className="lead mb-4 text-center">
                        Check your appointments or schedule today!
                    </p>
                    </div>
                    <div className="my-4 text-center">
                        <NavLink to='new' type="button" className="btn btn-primary btn-block btn-md m-2">Schedule an appointment</NavLink>
                    </div>
                </div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Is VIP?</th>
                            <th>Customer</th>
                            <th>Date/Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments.map(appointment => {
                            if (appointment.status === "canceled") {
                                return (null);
                            } else if (appointment.status === "finished") {
                                return (null);
                            } else {
                                return (
                                    <tr key={appointment.id}>
                                        <td>{appointment.vin}</td>
                                        <td>
                                            {automobiles.map(automobile => {
                                                if (appointment.vin === automobile.vin) {
                                                    return '✔️'
                                                } else {
                                                    return null;
                                                }
                                            })}
                                        </td>
                                        <td>{appointment.customer}</td>
                                        <td>{appointment.date_time}</td>
                                        <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                        <td>{appointment.reason}</td>
                                        <td>{appointment.status}</td>
                                        <td>
                                            <button onClick={() => {
                                                    fetch('http://localhost:8080/api/appointments/' + appointment.id + '/cancel', {method: 'PUT'})
                                                    .then(() => {
                                                        getData()
                                                    })
                                                    }} className="btn btn-danger btn-block btn-md m-2">Cancel</button>
                                            <button onClick={() => {
                                                    fetch('http://localhost:8080/api/appointments/' + appointment.id + '/finish', {method: 'PUT'})
                                                    .then(() => {
                                                        getData()
                                                    })
                                                    }} className="btn btn-success btn-block btn-md m-2">Finish</button>
                                        </td>
                                    </tr>
                                );
                            }
                        })}
                    </tbody>
                </table>
            </div>
    );
}
export default AppointmentList