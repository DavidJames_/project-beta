import { useEffect, useState } from 'react';

function SalespersonList() {
    const [salesperson, setSalesperson] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/')

        if (response.ok) {
            const data = await response.json()
            setSalesperson(data.salesperson)
        }
    }

    useEffect(()=>{
        getData()
    }, [])

    return (
        <div className="my-5">
            <div>
                <h1 className="display-5 fw-bold text-center">Salespeople</h1>
                <div className="col-lg-6 mx-auto">
                <p className="lead mb-4 text-center">
                    Our highly-valued sales team
                </p>
                </div>
            </div>
            <div key='container' className='container'>
                <table key='salesperson table' className='table'>
                    <thead>
                        <tr>
                            <th>Employee ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {salesperson.map(salesperson => {
                            return (
                                <tr key={salesperson.href}>
                                    <td>{salesperson.employee_id}</td>
                                    <td>{salesperson.first_name}</td>
                                    <td>{salesperson.last_name}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default SalespersonList
