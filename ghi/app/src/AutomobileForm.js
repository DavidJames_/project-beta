import React, {useEffect, useState,} from 'react';
import { useNavigate } from 'react-router-dom';

function AutomobileForm() {
    const [vin, setVin] = useState('')
    const [models, setModels] = useState([])
    const [model, setModel] = useState('')
    const [year, setYear] = useState('')
    const [color, setColor] = useState('')
    const navigate = useNavigate()

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.vin = vin
        data.model_id = model
        data.year = year
        data.color = color

        const automobilesUrl = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(automobilesUrl, fetchConfig)
        if (response.ok) {
            const newAutomobile = await response.json()
            console.log(newAutomobile)

            setVin('')
            setModel('')
            setYear('')
            setColor('')
            navigate('/automobiles')
        }
    }

    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
    }
    const handleModelChange = (event) => {
        const value = event.target.value
        setModel(value)
    }
    const handleYearChange = (event) => {
        const value = event.target.value
        setYear(value)
    }
    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/'

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setModels(data.models)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div key='form container' className='my-5 text-center container'>
            <div key='row' className='row'>
                <div key='column' className='offset-3 col-6'>
                    <div key='card' className='shadow p-4 mt-4'>
                        <h1>Add an Automobile</h1>
                        <form onSubmit={handleSubmit} id='create-automobile-form'>
                            <div key='vinform' className='form-floating mb-3'>
                                <input value={vin} onChange={handleVinChange} placeholder='VIN' required type='text' name='vin' id='vin' className='form-control'></input>
                                <label htmlFor='vin'>VIN</label>
                            </div>
                            <div key='modelform' className='form-floating mb-3'>
                                <select value={model} onChange={handleModelChange} required name="model" id="model" className="form-select" >
                                <option value=''>Choose a Model</option>
                                {models.map(model => {
                                    return (
                                        <option key={model.id} value={model.id}>
                                            {model.name}
                                        </option>
                                    )
                                })}
                                </select>
                            </div>
                            <div key='yearform' className='form-floating mb-3'>
                                <input value={year} onChange={handleYearChange} placeholder='year' required type='text' name='year' id='year' className='form-control'></input>
                                <label htmlFor='year'>Year</label>
                            </div>
                            <div key='colorform' className='form-floating mb-3'>
                                <input value={color} onChange={handleColorChange} placeholder='color' required type='text' name='color' id='color' className='form-control'></input>
                                <label htmlFor='color'>Color</label>
                            </div>
                            <button className='btn btn-primary'>Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AutomobileForm
