import { useEffect, useState } from 'react';

function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([])

    const getManufacturerData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/')

        if (response.ok) {
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }
    }

    useEffect(()=>{
        getManufacturerData()
    }, [])

    return (
        <div className="my-5">
            <div>
                <h1 className="display-5 fw-bold text-center">Manufacturers</h1>
                <div className="col-lg-6 mx-auto">
                <p className="lead mb-4 text-center">
                    Our wide selection of manufacturers
                </p>
                </div>
            </div>
            <div key='container' className='container'>
                <table key='manufacturer table' className='table'>
                    <thead>
                        <tr>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {manufacturers.map(manufacturer => {
                            return (
                                <tr key={manufacturer.href}>
                                    <td>{manufacturer.name}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default ManufacturerList
