import { useEffect, useState } from 'react';

function SalespersonListHistory() {
    const [salespeople, setSalespeople] = useState([])
    const [salesperson, setSalesperson] = useState('')
    const [sale, setSale] = useState([])
    const[filtersales, setFilterSales] = useState([])

    const handleSalespersonChange = async event => {
        const value = event.target.value
        setSalesperson(value)
        setFilterSales(sale.filter(sale => sale.salesperson.id === Number(value)))
    }

    const fetchSalespeopleData = async () => {
        const url = 'http://localhost:8090/api/salespeople/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setSalespeople(data.salesperson)
        }
    }

    const fetchSalesData = async () => {
        const url = 'http://localhost:8090/api/sales/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setSale(data.sale)
        }
    }

    useEffect(() => {
        fetchSalespeopleData()
        fetchSalesData()
    }, [])


    return (
        <div className="my-5">
            <div>
                <h1 className="display-5 fw-bold text-center">Salesperson History</h1>
            </div>
            <div key='form container' className='my-5 text-center container'>
                <select value={salesperson} onChange={handleSalespersonChange} required name = 'salesperson' id='salesperson' className='form-select'>
                    <option value=''>Choose a Salesperson</option>
                    {salespeople.map(salesperson => {
                        return (
                            <option key={salesperson.id} value={salesperson.id}>
                                {salesperson.first_name} {salesperson.last_name}
                            </option>
                        )
                    })}
                </select>
                <table key='saleshistory table' className='table'>
                    <thead>
                        <tr>
                            <th>Salesperson</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {filtersales.map(sale => (
                            <tr key={sale.id}>
                                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>${sale.price}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default SalespersonListHistory
