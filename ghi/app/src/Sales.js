import { useEffect, useState } from 'react';

function SaleList() {
    const [sale, setSale] = useState([])

    const getSalesData = async () => {
        const response = await fetch('http://localhost:8090/api/sales/')

        if (response.ok) {
            const data = await response.json()
            setSale(data.sale)
        }
    }

    useEffect(()=>{
        getSalesData()
    }, [])

    return (
        <div className="my-5">
            <div>
                <h1 className="display-5 fw-bold text-center">Sales</h1>
                <div className="col-lg-6 mx-auto">
                <p className="lead mb-4 text-center">
                    $$$
                </p>
                </div>
            </div>
            <div key='container' className='container'>
                <table key='sale table' className='table'>
                    <thead>
                        <tr>
                            <th>Salesperson Employee ID</th>
                            <th>Salesperson</th>
                            <th>Customer</th>
                            <th>Automobile</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {sale.map(sale => {
                            return (
                                <tr key={sale.href}>
                                    <td>{sale.salesperson.employee_id}</td>
                                    <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                    <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                    <td>{sale.automobile.vin}</td>
                                    <td>${sale.price}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default SaleList
