import React, {useEffect, useState,} from 'react';
import { useNavigate } from 'react-router-dom';

function SalesForm() {
    const[salespeople, setSalespeople] = useState([])
    const[customers, setCustomers] = useState([])
    const[automobiles, setAutomobiles] = useState([])
    const[salesperson, setSalesperson] = useState('')
    const[customer, setCustomer] = useState('')
    const[automobile, setAutomobile] = useState('')
    const[price, setPrice] = useState('')
    const navigate = useNavigate()

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.salesperson = salesperson
        data.customer = customer
        data.automobile = automobile
        data.price = price


        const salesUrl = 'http://localhost:8090/api/sales/'
        const automobileUrl = `http://localhost:8100/api/automobiles/${automobile}/`

        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const updateSoldStatus = {
            method: 'PUT',
            body: JSON.stringify({ sold: true }),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const salesResponse = await fetch(salesUrl, fetchConfig)



        console.log(salesResponse)
        if (salesResponse.ok) {
            const newSale = await salesResponse.json()
            console.log(newSale)

            const updateSoldResponse = await fetch(automobileUrl, updateSoldStatus)
            if (updateSoldResponse.ok)
            console.log('Sold updated successfully')

            setSalesperson('')
            setCustomer('')
            setAutomobile('')
            setPrice('')
            navigate('/sales')
        } else {
            console.log(salesResponse)
        }


        }

        const fetchSalespeople = async () => {
            const url='http://localhost:8090/api/salespeople/'

            const response = await fetch(url)

            if (response.ok) {
                const data = await response.json()
                setSalespeople(data.salesperson)
            }
        }
        const fetchCustomers = async () => {
            const url='http://localhost:8090/api/customers/'

            const response = await fetch(url)

            if (response.ok) {
                const data = await response.json()
                setCustomers(data.customer)
            }
        }
        const fetchAutomobiles = async () => {
            const url='http://localhost:8100/api/automobiles/'

            const response = await fetch(url)

            if (response.ok) {
                const data = await response.json()
                setAutomobiles(data.autos)
            }
        }

        const handleSalespersonChange = (event) => {
            const value = event.target.value
            setSalesperson(value)
        }
        const handleCustomerChange = (event) => {
            const value = event.target.value
            setCustomer(value)
        }
        const handleAutomobileChange = (event) => {
            const value = event.target.value
            setAutomobile(value)
        }
        const handlePriceChange = (event) => {
            const value = event.target.value
            setPrice(value)
        }

        useEffect(() => {
            fetchSalespeople()
            fetchCustomers()
            fetchAutomobiles()
        }, [])

        const availableAutomobiles = automobiles.filter(automobile => !automobile.sold)

        return (
            <div key='form container' className='my-5 text-center container'>
                <div key='row' className='row'>
                    <div key='column' className='offset-3 col-6'>
                        <div key='card' className='shadow p-4 mt-4'>
                            <h1>Record a new sale</h1>
                            <form onSubmit={handleSubmit} id='create-sale-form'>
                                <div key='salespersonform' className='mb-3'>
                                    <label htmlfor='salesperson'>Salesperson</label>
                                    <select value={salesperson} onChange={handleSalespersonChange} required name='salesperson' id='salesperson' className='form-select'>
                                        <option value=''>Choose a salesperson...</option>
                                        {salespeople.map(salesperson => {
                                            return (
                                                <option key={salesperson.id} value={salesperson.id}>
                                                    {salesperson.first_name} {salesperson.last_name}
                                                </option>
                                            )
                                        })}
                                    </select>
                                </div>
                                <div key='customerform' className='mb-3'>
                                    <label htmlfor='customer'>Customer</label>
                                    <select value={customer} onChange={handleCustomerChange} required name='customer' id='customer' className='form-select'>
                                        <option value=''>Choose a customer...</option>
                                        {customers.map(customer => {
                                            return (
                                                <option key={customer.id} value={customer.id}>
                                                    {customer.first_name} {customer.last_name}
                                                </option>
                                            )
                                        })}
                                    </select>
                                </div>
                                 <div key='automobileform' className='mb-3'>
                                 <label htmlfor='automobile'>Automobile VIN</label>
                                    <select value={automobile} onChange={handleAutomobileChange} required name='automobile' id='automobile' className='form-select'>
                                        <option value=''>Choose an automobile VIN...</option>
                                        {availableAutomobiles.map(automobile => {
                                            return (
                                                <option key={automobile.vin} value={automobile.vin}>
                                                    {automobile.vin}
                                                </option>
                                            )
                                        })}
                                    </select>
                                </div>
                                <div key='priceform' className='mb-3'>
                                    <label htmlfor='price'>Price</label>
                                    <input className='form-control' value={price} onChange={handlePriceChange} required type='number' placeholder='0' id='price'></input>
                                </div>
                                <button className='btn btn-primary'>Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )

}

export default SalesForm
