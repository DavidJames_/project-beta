import { useEffect, useState } from 'react';

function CustomerList() {
    const [customer, setCustomer] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/customers/')

        if (response.ok) {
            const data = await response.json()
            setCustomer(data.customer)
        }
    }

    useEffect(()=>{
        getData()
    }, [])

    return (
        <div className="my-5">
            <div>
                <h1 className="display-5 fw-bold text-center">Customers</h1>
                <div className="col-lg-6 mx-auto">
                <p className="lead mb-4 text-center">
                    We thank you for your business!
                </p>
                </div>
            </div>
            <div key='container' className='container'>
                <table key='customer table' className='table'>
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Phone Number</th>
                            <th>Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        {customer.map(customer => {
                            return (
                                <tr key={customer.href}>
                                    <td>{customer.first_name}</td>
                                    <td>{customer.last_name}</td>
                                    <td>{customer.phone_number}</td>
                                    <td>{customer.address}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default CustomerList
