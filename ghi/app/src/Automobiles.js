import { useEffect, useState } from 'react';

function AutomobileList() {
    const [automobiles, setAutomobiles] = useState([])

    const fetchData = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/')

        if (response.ok) {
            const data = await response.json()
            setAutomobiles(data.autos)
        }
    }

    useEffect(()=>{
        fetchData()
    }, [])

    return (
        <div className="my-5">
            <div>
                <h1 className="display-5 fw-bold text-center">Automobiles</h1>
                <div className="col-lg-6 mx-auto">
                <p className="lead mb-4 text-center">
                    Our current inventory
                </p>
                </div>
            </div>
            <div key='container' className='container'>
                <table key='automobile table' className='table'>
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Model</th>
                            <th>Manufacturer</th>
                            <th>Year</th>
                            <th>Color</th>
                            <th>Sold</th>
                        </tr>
                    </thead>
                    <tbody>
                        {automobiles.map(automobile => {
                            return (
                                <tr key={automobile.id}>
                                    <td>{automobile.vin}</td>
                                    <td>{automobile.model.name}</td>
                                    <td>{automobile.model.manufacturer.name}</td>
                                    <td>{automobile.year}</td>
                                    <td>{automobile.color}</td>
                                    <td>{automobile.sold ? '✔️' : '❌'}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default AutomobileList
