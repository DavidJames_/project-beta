import React, {useState,} from 'react';
import { useNavigate } from 'react-router-dom';

function SalespersonForm() {
    const [employeeID, setEmployeeID] = useState('')
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const navigate = useNavigate()

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.employee_id = employeeID
        data.first_name = firstName
        data.last_name = lastName

        const salespeopleUrl = 'http://localhost:8090/api/salespeople/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(salespeopleUrl, fetchConfig)
        if (response.ok) {
            const newSalesperson = await response.json()
            console.log(newSalesperson)

            setEmployeeID('')
            setFirstName('')
            setLastName('')
            navigate('/salespeople')
        }
    }

    const handleEmployeeIDChange = (event) => {
        const value = event.target.value
        setEmployeeID(value)
    }
    const handleFirstNameChange = (event) => {
        const value = event.target.value
        setFirstName(value)

    }
    const handleLastNameChange = (event) => {
        const value = event.target.value
        setLastName(value)
    }

    return (
        <div key='form container' className='my-5 text-center container'>
            <div key='row' className='row'>
                <div key='column' className='offset-3 col-6'>
                    <div key='card' className='shadow p-4 mt-4'>
                        <h1>Add a Salesperson</h1>
                        <form onSubmit={handleSubmit} id='create-salesperson-form'>
                            <div key='employeeidform' className='form-floating mb-3'>
                                <input value={employeeID} onChange={handleEmployeeIDChange} placeholder='Employee ID' required type='text' name='employee_id' id='employee_id' className='form-control'></input>
                                <label htmlFor='employee_id'>Employee ID</label>
                            </div>
                            <div key='firstnameform' className='form-floating mb-3'>
                                <input value={firstName} onChange={handleFirstNameChange} placeholder='First name' required type='text' name='first_name' id='first_name' className='form-control'></input>
                                <label htmlFor='first_name'>First name</label>
                            </div>
                            <div key='lastnameform' className='form-floating mb-3'>
                                <input value={lastName} onChange={handleLastNameChange} placeholder='Last name' required type='text' name='last_name' id='last_name' className='form-control'></input>
                                <label htmlFor='first_name'>Last name</label>
                            </div>
                            <button className='btn btn-primary'>Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default SalespersonForm
