import { useEffect, useState } from 'react';

function ModelsList() {
    const [models, setModels] = useState([])

    const getModelData = async () => {
        const response = await fetch('http://localhost:8100/api/models/')

        if (response.ok) {
            const data = await response.json()
            setModels(data.models)
        }
    }

    useEffect(()=>{
        getModelData()
    }, [])

    return (
        <div className="my-5">
            <div>
                <h1 className="display-5 fw-bold text-center">Models</h1>
                <div className="col-lg-6 mx-auto">
                <p className="lead mb-4 text-center">
                    Our wide selection of models
                </p>
                </div>
            </div>
            <div key='container' className='container'>
                <table key='model table' className='table table-striped'>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Manufacturer</th>
                            <th>Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                        {models.map(model => {
                            return (
                                <tr key={model.href}>
                                    <td>{model.name}</td>
                                    <td>{model.manufacturer.name}</td>
                                    <td><img width='360' height='240' className='img-thumbnail shadow-md' src={model.picture_url}></img></td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default ModelsList
