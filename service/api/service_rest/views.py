from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
import json
from .models import Technician, Appointment, AutomobileVO

# Create your views here.
class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = Technician
    properties = [
        "vin",
        "sold",
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
    ]
    encoders = {
        "technician": TechnicianDetailEncoder(),
    }


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
    ]
    encoders = {
        "technician": TechnicianDetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_technician_detail(request, id):
    if request.method == "GET":
        technician = Technician.objects.get(id=id)
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianDetailEncoder,
            safe=False,
        )
    else:
        try:
            count, _ = Technician.objects.filter(id=id).delete()
            return JsonResponse(
                {"deleted": count > 0},
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"Error": "Technician does not exist"},
                status=404,
            )


@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        try:
            technician_id = content["technician"]
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician id"},
                status=400,
            )

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_appointment_detail(request, id):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    else:
        try:
            count, _ = Appointment.objects.filter(id=id).delete()
            return JsonResponse(
                {"deleted": count > 0},
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"Error": "Appointment does not exist"},
                status=404,
            )


@require_http_methods(["PUT"])
def api_cancel_appointment(request, id):
    if request.method == "PUT":
        Appointment.objects.filter(id=id).update(status="canceled")
        return JsonResponse(
            {"status": "canceled"}, encoder=AppointmentDetailEncoder, safe=False
        )


@require_http_methods(["PUT"])
def api_finish_appointment(request, id):
    if request.method == "PUT":
        Appointment.objects.filter(id=id).update(status="finished")
        return JsonResponse(
            {"status": "finished"}, encoder=AppointmentDetailEncoder, safe=False
        )
